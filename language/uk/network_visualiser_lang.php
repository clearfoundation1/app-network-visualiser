<?php

$lang['network_visualiser_app_description'] = 'Додаток «Візуалізатор мережі» фіксує та відображає потік даних, який проходить через вашу мережу в режимі реального часу, відображаючи інформацію про джерело, призначення та протокол на додаток також використання пропускної здатності або загальних даних, зібраних за певний інтервал.';
$lang['network_visualiser_app_name'] = 'Візуалізатор мережі';
$lang['network_visualiser_display'] = 'Відображати';
$lang['network_visualiser_display_invalid'] = 'Display is invalid.';
$lang['network_visualiser_interval'] = 'Інтервал оновлення (секунди)';
$lang['network_visualiser_interval_is_invalid'] = 'Інтервал недійсний.';
$lang['network_visualiser_report'] = 'Формат звіту';
$lang['network_visualiser_report_detailed'] = 'Таблиця – Детальна';
$lang['network_visualiser_report_graphical'] = 'Графічний';
$lang['network_visualiser_report_invalid'] = 'Тип звіту недійсний.';
$lang['network_visualiser_report_simple'] = 'Таблиця - Проста';
$lang['network_visualiser_top_users'] = 'Найпопулярніші споживачі пропускної здатності';
$lang['network_visualiser_total_transfer'] = 'Загальна передача';
$lang['network_visualiser_traffic_summary'] = 'Мережевий трафік';
